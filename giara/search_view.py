from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from gi.repository import Gtk, Adw
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.sections import SubredditsSection
from giara.confManager import ConfManager


class SearchViewHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, stack, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/'
                'headerbar_with_back_and_squeezer.ui'
            ),
            section=None,
            stack=stack,
            **kwargs
        )
        self.back_btn = self.builder.get_object('back_btn')


class CommonSearchView(Gtk.Box):
    def __init__(self, headerbar, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit

        self.headerbar = headerbar
        self.searchbar = Gtk.SearchBar(
            vexpand=False, hexpand=True
        )
        self.search_entry = Gtk.SearchEntry()
        self.search_entry.connect('activate', self.on_search_activate)
        self.searchbar.set_child(self.search_entry)
        self.searchbar.connect_entry(self.search_entry)

        self.append(self.headerbar)
        self.append(self.searchbar)
        self.searchbar.set_search_mode(True)

    def on_search_activate(self, *args):
        raise NotImplementedError()


class SearchView(CommonSearchView):
    def __init__(self, show_post_func, show_subreddit_func, **kwargs):
        self.show_post_func = show_post_func
        self.show_subreddit_func = show_subreddit_func
        self.stack = Adw.ViewStack(
            vexpand=True
        )
        self.subreddits_section = SubredditsSection(
            None, self.show_subreddit_func, load_now=False
        )
        self.stack.add_titled(
            self.subreddits_section, 'subreddits', _('Subreddits')
        ).set_icon_name('org.gabmus.giara-symbolic')
        self.users_section = SubredditsSection(
            None, self.show_subreddit_func, load_now=False
        )
        self.stack.add_titled(
            self.users_section, 'users', _('Users')
        ).set_icon_name('avatar-default-symbolic')

        def on_stack_child_changed(*args):
            lbox = self.stack.get_visible_child().subreddit_listbox
            if lbox.generator is not None and not lbox.initialized:
                lbox.refresh()

        self.stack.connect(
            'notify::visible-child',
            on_stack_child_changed
        )
        super().__init__(
            SearchViewHeaderbar(self.stack),
            **kwargs
        )

        self.bottom_bar = Adw.ViewSwitcherBar(vexpand=False)
        self.bottom_bar.set_stack(self.stack)
        self.headerbar.connect(
            'squeeze',
            lambda caller, squeezed: self.bottom_bar.set_reveal(squeezed)
        )
        self.append(self.stack)
        self.append(self.bottom_bar)

    def on_search_activate(self, *args):
        term = self.search_entry.get_text()
        subs_view = self.subreddits_section.subreddit_listbox
        subs_view.set_gen_func(
            lambda *args, **kwargs: self.reddit.subreddits.search(
                term, *args, **kwargs
            )
        )
        subs_view.initialized = False
        users_view = self.users_section.subreddit_listbox
        users_view.set_gen_func(
            lambda *args, **kwargs: self.reddit.redditors.search(
                term, *args, **kwargs
            )
        )
        users_view.initialized = False
        self.stack.get_visible_child().subreddit_listbox.refresh()
