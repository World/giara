from giara.download_manager import download_img
from giara.path_utils import is_image


def get_subreddit_icon(sub):
    if sub is None:
        return
    icon = None
    icon = getattr(sub, 'icon_img', getattr(sub, 'community_icon', None))
    if is_image(icon):
        return download_img(icon)


def show_hide_gallery_btns(pos, tot_len, prev_w, next_w):
    prev_w.set_visible(pos > 0)
    next_w.set_visible(pos < tot_len - 1)
