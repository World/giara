from gi.repository import Gtk
from giara.sections import PostListSection
from giara.single_post_stream_headerbar import SinglePostStreamHeaderbar


class SinglePostStreamView(Gtk.Box):
    def __init__(self, generator, name, title, show_post_func, load_now=True,
                 source=None, sort_menu=True, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.first_refresh_done = False
        self.generator = generator
        self.name = name
        self.title = title

        self.section = PostListSection(
            self.generator,
            show_post_func,
            source=source,
            load_now=load_now
        )
        self.listview = self.section.listview
        self.headerbar = SinglePostStreamHeaderbar(
            self.title,
            self.listview if sort_menu else None
        )
        self.append(self.headerbar)
        self.append(self.section)

        self.headerbar.refresh_btn.connect(
            'clicked',
            self.refresh
        )

    def refresh(self, *args):
        self.listview.refresh()

    def first_refresh(self):
        if self.first_refresh_done:
            return
        self.first_refresh_done = True
        self.refresh()
