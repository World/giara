<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
    <id>org.gabmus.giara</id>
    <name>Giara</name>
    <developer_name>Gabriele Musco</developer_name>
    <summary>An app for Reddit</summary>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0+</project_license>
    <!--recommends>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </recommends-->
    <description>
        <p>An app for Reddit.</p>
        <p>Browse Reddit from your Linux desktop or smartphone.</p>
    </description>
    <launchable type="desktop-id">org.gabmus.giara.desktop</launchable>
    <screenshots>
        <screenshot type="default">
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/mainwindow.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot01.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot02.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot03.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot04.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot05.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot06.png</image>
            <image>https://gitlab.gnome.org/World/giara/raw/website/static/screenshots/scrot07.png</image>
        </screenshot>
    </screenshots>
    <url type="homepage">https://giara.gabmus.org</url>
    <url type="bugtracker">https://gitlab.gnome.org/World/giara/-/issues</url>
    <url type="translate">https://gitlab.gnome.org/World/giara/-/tree/master/po</url>
    <url type="donation">https://liberapay.com/gabmus/donate</url>
    <update_contact>gabmus@disroot.org</update_contact>
    <releases>
        <release version="1.1.0" timestamp="1679827407">
            <description>
                <ul>
                    <li>Update to latest GNOME stack</li>
                    <li>Various bug fixes</li>
                </ul>
            </description>
        </release>

        <release version="1.0.1" timestamp="1649928080">
            <description>
                <ul>
                    <li>Updated dependencies</li>
                    <li>Miscellaneous improvements and fixes</li>
                </ul>
            </description>
        </release>

        <release version="1.0" timestamp="1630313800">
            <description>
                <ul>
                    <li>New version 1.0 based on GTK4 and libadwaita!</li>
                    <li>Optimized post lists based on ListView</li>
                    <li>New and improved picture view with built-in image viewer</li>
                    <li>Added support for picture galleries</li>
                    <li>NSFW images are now blurred</li>
                    <li>Added support for custom emojis in post flairs</li>
                    <li>Many improvements and bug fixes</li>
                </ul>
            </description>
        </release>

        <release version="0.3" timestamp="1605683256">
            <description>
                <ul>
                    <li>Opening a post from a comment jumps to that comment</li>
                    <li>Post creation and editing are now asynchronous</li>
                    <li>Replaced quit button in welcome view with a more standard close button</li>
                    <li>Fixed icon rendering problems in KDE</li>
                    <li>Comment replies can now be collapsed</li>
                    <li>Comments don't always load in full, hidden comments can be loaded if the user wants them</li>
                    <li>You can now open Twitter links in Nitter and YouTube links in Invidious</li>
                    <li>Limited picture preview size</li>
                    <li>Added all available post sorting options</li>
                    <li>New UI for choosing post sorting</li>
                    <li>Implemented blockquote rendering</li>
                    <li>Implemented superscript rendering</li>
                    <li>Improved Markdown rendering</li>
                    <li>Improved Inbox view</li>
                    <li>Added A LOT of new languages! Thanks a lot to the community for the contributions!</li>
                </ul>
            </description>
        </release>
        <release version="0.2" timestamp="1603273834">
            <description>
                <ul>
                    <li>Improved markdown rendering</li>
                    <li>Dark mode implemented</li>
                    <li>Added option to disable images in post previews</li>
                    <li>Added option to set a maximum size for images</li>
                    <li>Added option to clear cache</li>
                    <li>Added Brazilian Portuguese translation</li>
                    <li>New post or comment window now remembers its size</li>
                    <li>Subreddits can now be searched when creating new posts</li>
                    <li>Initial support for multireddits</li>
                    <li>Added Croatian translation</li>
                    <li>Added notifications for unread inbox items</li>
                    <li>Made comment border lines colorful</li>
                    <li>Authentication is now handled by your default browser</li>
                </ul>
            </description>
        </release>
        <release version="0.1.1" timestamp="1602403522">
            <description>
                <ul>
                    <li>Improvements for flatpak packaging</li>
                </ul>
            </description>
        </release>
        <release version="0.1" timestamp="1589880777">
            <description>
                <ul>
                    <li>First release</li>
                </ul>
            </description>
        </release>
    </releases>
    <content_rating type="oars-1.1">
        <content_attribute id="social-chat">moderate</content_attribute>
    </content_rating>
    <custom>
        <value key="Purism::form_factor">workstation</value>
        <value key="Purism::form_factor">mobile</value>
    </custom>
</component>
